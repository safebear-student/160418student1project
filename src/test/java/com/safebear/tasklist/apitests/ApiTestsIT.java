package com.safebear.tasklist.apitests;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.get;

public class ApiTestsIT {

    final String DOMAIN = System.getProperty("domain");
    final int PORT = Integer.parseInt(System.getProperty("port"));
    final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        RestAssured.baseURI=DOMAIN;
        RestAssured.port=PORT;

        RestAssured.registerParser("application/json", Parser.JSON);
    }


    // 11. When this runs it goes to 'http://localhost:8080' rather than 'http://localhost:8080/safebear'. Why?
    // Context was missing so it couldn't construct the URL correctly
    @Test
    public void testTasksEndPoint(){

        get(CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

        // 4. Also when it runs it says the status code is wrong. What should be the correct one?
        // Should be 200OK
    }

}
