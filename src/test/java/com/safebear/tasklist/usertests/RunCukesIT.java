package com.safebear.tasklist.usertests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        // 8. Why isn't this running any of my scenarios?
        // Because you've told it not to run anything tagged with @high-risk using the ~ and your only test is tagged as that.
        // Changed it to ~@to-do as this is more useful'

        plugin = {"pretty", "html:target/cucumber"},
        tags = "~@to-do",
        glue = "com.safebear.tasklist.usertests",
        features = "classpath:tasklist.features/taskmanagement.feature"
)
public class RunCukesIT {
}
