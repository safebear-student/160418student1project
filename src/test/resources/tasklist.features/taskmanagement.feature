Feature: Task Management

  User Story:

  In order to manage my tasks
  As a user
  I need to be able to add, delete, update tasks

  Rules / Acceptance criteria:

   - You must be able to view your tasks
   - They must be paginated

  Questions:
   - How many tasks to display on screen at once?

  To do:
   - Pagination

  Domain language:

  Task = A task is made up of a name, a due date and a status
  Status = not completed / competed

  # Our only test

  @high-risk
  @high-impact
  Scenario Outline: A user creates a task

#    14. Why is this step not matching any methods in the StepDefs file?
    # It uses regex to match and the text was different

    When a user creates a <task>
    Then the <task> appears in the tasklist
    Examples:
      | task |
      |Clean the windows|
